export { default as Home } from './home'
export { default as People } from './people'
export { default as Todos } from './todos'