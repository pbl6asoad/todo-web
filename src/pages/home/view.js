import React from 'react'
import { TodoList } from '../../components'
import './style.scss'

const View = (props) => {
    
    return (
        <div>
            <p>{props.userData.name}</p>
            <TodoList list={props.todo.todoList} loading={props.todo.loading}/> 
        </div>
    )
}

export default View