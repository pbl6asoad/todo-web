/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import View from './view';
import { setUserData } from '../../store/reducers/user-reducer';
import { setTodoList } from '../../store/reducers/todo-reducer';
import { connect } from 'react-redux';


const Container = (props) => {
  useEffect(() => {
    props.setUserData({name: 'Ilya'})
    props.setTodolist([{name: 'Ilya'}])
  }, [])
  
  return (
    <View userData={props.userData} todo={props.todo}/>
  );
}


const mapStateToProps = state => ({
  userData: state.user.userData,
  todo: state.todo,
});
const mapDispatchToProps = dispatch => ({
  setUserData: userData => dispatch(setUserData(userData)),
  setTodolist: todoList => dispatch(setTodoList(todoList)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);