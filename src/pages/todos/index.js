/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import View from './view';
import { connect } from 'react-redux';


const Container = (props) => {
  
  return (
    <View todo={props.todo}/>
  );
}


const mapStateToProps = state => ({
  todo: state.todo,
});
const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);