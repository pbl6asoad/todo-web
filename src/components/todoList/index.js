import React from 'react'
import { Loader } from '../../ui-kit'
import { ListItem } from '..'
import './style.scss'

const Component = (props) => {
    
    return (
        <div>
            {props.loading ? (
                <Loader />
            ) : (
                <>
                {props.list.map(item => <ListItem key={item} item={item}/>)}
                </>
            )}
        </div>
    )
}

export default Component