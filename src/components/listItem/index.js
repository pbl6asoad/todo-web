import React from 'react'

import './style.scss'

const Component = (props) => {
    
    return (
        <div className="todo__item">
            <p>{props.item.name}</p>
        </div>
    )
}

export default Component