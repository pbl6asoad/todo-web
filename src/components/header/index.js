import React from 'react'

import './style.scss'

const Component = (props) => {
    
    return (
        <header className="header">
            <p>{props.data.name}</p>
        </header>
    )
}

export default Component