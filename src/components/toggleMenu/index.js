import React, { useState} from 'react'
// import Icon from '../../ui-kit/icons'
import { useTranslation } from 'react-i18next';
import { Icon } from '../../ui-kit'
import Tooltip from '@material-ui/core/Tooltip';
import { NavLink } from 'react-router-dom';
import './style.scss'


const Component = (props) => {
    const [isOpen, toggleMenu] = useState(true)
    const { t } = useTranslation()
    const MenuItems = [
      {
        name: t('MENU_HOME'),
        link: 'home',
        icon: 'home',
        active: 'home-filled'
      },
      {
        name: t('MENU_PEOPLE'),
        link: 'people',
        icon: 'people',
        active: 'people-filled'
      },

      {
        name: t('MENU_TODOS'),
        link: 'todos',
        icon: 'todos',
        active: 'todos-filled'
      }
    ]
    return (
        <div className={`toggle-menu ${isOpen ? 'opened' : 'closed'}`}> 
          {/* <button onClick={() => i18next.changeLanguage('ru')}>Ru lang</button>
          <button onClick={() => i18next.changeLanguage('en')}>En lang</button> */}
          <h1 className="toggle-menu__project-name" onClick={() => toggleMenu(prevState => !prevState)}>TODO</h1>
          <div className="toggle-menu__items-container">
            {MenuItems.map(item => 
            <Tooltip title={item.name} key={item.link} disableHoverListener={isOpen} placement="right">
              <NavLink className="toggle-menu__item" to={item.link} activeClassName="toggle-menu__item-active">
                  <Icon type={item.icon} />
                {isOpen && item.name}
              </NavLink>
            </Tooltip>
            )}
          </div>
        </div>
    )
}

export default Component