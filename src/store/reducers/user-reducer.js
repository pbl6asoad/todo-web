const SET_USER_DATA = 'SET_USER_DATA';
const LOGOUT = 'LOGOUT';

const initialState = {
  userData: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return {
        ...state,
        userData: action.userData,
      };
    case LOGOUT:
      return {
        ...state,
        userData: null,
      };
    default:
      return state;
  }
};

export default userReducer;

export const setUserData = userData => {
  return dispatch => {
    dispatch({ type: SET_USER_DATA, userData });
  };
};

export const logOut = () => {
  return dispatch => {
    dispatch({ type: LOGOUT });
  };
};
