const ADD_TODO_ITEM = 'ADD_TODO_ITEM';
const SET_TODO_LIST = 'SET_TODO_LIST';

const initialState = {
  todoList: [],
  loading: true,
  success: false,
  fail: false,
  last: false,
};

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO_ITEM:
      return {
        ...state,
        todoList: [...state.todoList, action.item],
        loading: false,
      };
    case SET_TODO_LIST:
      return {
        ...state,
        todoList: action.todoList,
        loading: false,
      };
    default:
      return state;
  }
};

export default todoReducer;

export const addTodoItem = userData => {
  return dispatch => {
    dispatch({ type: ADD_TODO_ITEM, todoItem: userData });
  };
};

export const setTodoList = todoList => {
  return dispatch => {
    dispatch({ type: SET_TODO_LIST, todoList });
  };
};
