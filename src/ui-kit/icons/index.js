import React from 'react';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';
import HomeIcon from '@material-ui/icons/Home';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import PeopleIcon from '@material-ui/icons/People';
import ListAltOutlinedIcon from '@material-ui/icons/ListAltOutlined';
import ListIcon from '@material-ui/icons/List';
import './style.scss';

const Icon = (props) => {
  switch(props.type){
    default: 
    case 'home-filled':
      return <HomeIcon />
    case 'home':
      return <HomeOutlinedIcon />
    case 'people-filled':
      return <PeopleIcon />
    case 'people':
      return <PeopleAltOutlinedIcon />
    case 'todos-filled':
      return <ListIcon />
    case 'todos':
      return <ListAltOutlinedIcon />
  }
}

export default Icon