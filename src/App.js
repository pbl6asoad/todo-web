import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { Home, People, Todos } from './pages'
import { ToggleMenu, Header } from './components'

function App() {
  return (
    <>
      <ToggleMenu />
      <div className="content">        
        <Header data={{name: 'Ilya'}}/>
        <Router>
          <div className="page__container">
            <Switch>
              <Route path="/home">
                <Home />
              </Route>
              <Route path="/todos">
                <Todos />
              </Route>
              <Route path="/people">
                <People />
              </Route>
            </Switch>
          </div>
        </Router>
      </div>
    </>
  );
}

export default App;
